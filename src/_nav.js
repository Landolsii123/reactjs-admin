export default {
  items: [
    {
      name: 'Category',
      url: '/home/category',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
      children:[{
        name: 'Category',
        url: '/home/category',
        icon: 'icon-speedometer',
      },
        {
        name: 'Add Category',
        url: '/home/addCategory',
        icon: 'icon-speedometer',
      },
      ]
    },
    {
      name: 'Reclamation',
      url: '/home/reclamation',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
    },
    {
      name: 'Reservation',
      url: '/home/reservation',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
    },
    {
      name: 'Annonce',
      url: '/home/annonce',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
      children:[{
        name: 'Annonce',
        url: '/home/annonce',
        icon: 'icon-speedometer',
      },
        {name: 'Add Annonce',
          url: '/home/addAnnonce',
          icon: 'icon-speedometer',}
          ]
    },
  ],
};
