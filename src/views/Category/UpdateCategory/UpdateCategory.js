import React, { Component } from 'react';
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      catName:"",
      category:{catName:""}
    };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }


  componentDidMount(){
    console.log("update")
    this.getCat();
  }

  getCat(){
    fetch('http://localhost:8080/Categorie/one/'+localStorage.getItem("idCat"), {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({category: data});
      });
  }

  updateCategory(){
    if(this.state.catName === ""){
      this.state.catName = this.state.category.catName;
    }
    else {
      axios.put("http://localhost:8080/Categorie/update/"+localStorage.getItem("idCat") , {
        catName: this.state.catName
      }).then(res => {
        console.log("data ",res.data);
        window.location.href="/#/home/category"
      })
    }

  }

  render() {
    return (
      <div className="animated fadeIn">

        <Row>

          <Col xs="12" md="6">
            <Card>
              <CardHeader>
                <strong>Category</strong> Update
              </CardHeader>
              <CardBody>
                <Form action="" method="post" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Name</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="Text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             defaultValue={this.state.category.catName}
                             onChange={evt=>this.setState({catName:evt.target.value})}
                      />
                      <FormText className="help-block">Please Enter The Name</FormText>
                    </Col>
                  </FormGroup>

                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.updateCategory.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
