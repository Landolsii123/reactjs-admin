import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';

let prev = 0;

let next = 0;

let last = 0;

let first = 0;

class Tables extends Component {
  constructor(){
    super();
    this.state={reclamation:[],
      currentPage: 1,
      todosPerPage: 5
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleLastClick = this.handleLastClick.bind(this);
    this.handleFirstClick = this.handleFirstClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  handleLastClick(event) {
    event.preventDefault();
    this.setState({
      currentPage:last
    });
  }

  handleFirstClick(event) {
    event.preventDefault();
    this.setState({
      currentPage:1
    });
  }

  componentDidMount(){
    this.getAllReclamation();
  }

  getAllReclamation(){
    fetch('http://localhost:8080/Reclamation/all', {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({reclamation: data});
      });
  }

  getRec(){
    fetch('http://localhost:8080/Reclamation/one/'+localStorage.getItem("idCat"), {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({category: data});
      });
  }



  handleClickResponse(e,id){
    e.preventDefault();
    console.log("id ",id)
    localStorage.setItem("idRec",id);
    window.location.href="/#/home/responseReclamation"
  }

  render() {
    let {reclamation, currentPage, todosPerPage} = this.state;
    // Logic for displaying current todos
    let indexOfLastTodo = currentPage * todosPerPage;
    let indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    let currentTodos = reclamation.slice(indexOfFirstTodo, indexOfLastTodo);
    prev = currentPage > 0 ? (currentPage - 1) : 0;
    last = Math.ceil(reclamation.length / todosPerPage);
    next = (last === currentPage) ? currentPage : currentPage + 1;
    // Logic for displaying page numbers
    let pageNumbers = [];
    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }
    return (
      <div className="animated fadeIn">
        <Row>


          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Reclamation List
              </CardHeader>
              <CardBody>
                <Table responsive striped>
                  <thead>
                  <tr>
                    <th>Reclamation date</th>
                    <th>Description</th>

                    <th>Answer</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    currentTodos.map((item,index) =>{
                      return(
                        <tr key={index}>

                        <td>{item.dateRec}</td>
                        <td>{item.description}</td>
                          {
                            item.responded === "0"?
                              <td><i class="fas fa-reply-all" onClick={evt=>this.handleClickResponse(evt,item.id)}></i></td>
                              :null
                          }
                          {
                            item.responded === "1"?
                              <td><i class="fas fa-reply-all" style={{color:"green"}}></i></td>
                              :null
                          }

                      </tr>
                          );
                          })
                          }
                  </tbody>
                </Table>
                <nav>
                  <Pagination>
                    <PaginationItem>

                      { prev === 0 ? <PaginationLink disabled>First</PaginationLink> :

                        <PaginationLink onClick={this.handleFirstClick} id={prev} href={prev}>First</PaginationLink>

                      }

                    </PaginationItem>

                    <PaginationItem>

                      { prev === 0 ? <PaginationLink disabled>Prev</PaginationLink> :

                        <PaginationLink onClick={this.handleClick} id={prev} href={prev}>Prev</PaginationLink>

                      }

                    </PaginationItem>

                    {

                      pageNumbers.map((number,i) =>

                        <Pagination key= {i}>

                          <PaginationItem active = {pageNumbers[currentPage-1] === (number) ? true : false} >

                            <PaginationLink onClick={this.handleClick} href={number} key={number} id={number}>

                              {number}

                            </PaginationLink>

                          </PaginationItem>

                        </Pagination>

                      )}



                    <PaginationItem>

                      {

                        currentPage === last ? <PaginationLink disabled>Next</PaginationLink> :

                          <PaginationLink onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Next</PaginationLink>

                      }

                    </PaginationItem>



                    <PaginationItem>

                      {

                        currentPage === last ? <PaginationLink disabled>Last</PaginationLink> :

                          <PaginationLink onClick={this.handleLastClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Last</PaginationLink>

                      }

                    </PaginationItem>

                  </Pagination>

                </nav>
              </CardBody>
            </Card>
          </Col>
        </Row>


      </div>

    );
  }
}

export default Tables;
