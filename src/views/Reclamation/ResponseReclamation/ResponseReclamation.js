import React, { Component } from 'react';
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      description:"",
      reclamation:{description:""},
      to:"",
      content:""
    };
  }

  componentDidMount(){
    this.getRec();

  }

  setReponse(){
    fetch('http://localhost:8080/Reclamation/setResponse/'+localStorage.getItem("idRec"), {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
      });

  }


  getRec(){
    fetch('http://localhost:8080/Reclamation/one/'+localStorage.getItem("idRec"), {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({reclamation: data});
        this.setState({to: data['client']['email']});
      });
  }

  responseReclamation(){

      axios.post("http://localhost:8080/Reservation/sendMail/", {
      }).then(res => {
        this.setReponse();
        console.log("data ",res.data);
       window.location.href="/#/home/reservation";
      })


  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }

  render() {
    return (
      <div className="animated fadeIn">

        <Row>
          <Col xs="12" md="12">

            <Card>
              <CardHeader>
                <strong>Response</strong> Reclamation
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="nf-email">Reclamation</Label>
                    <Input type="email" id="nf-email" name="nf-email" autoComplete="email"
                           value={this.state.reclamation.description}
                    />
                    <FormText className="help-block">Client Reclamation</FormText>
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="nf-password">Response</Label>
                    <Input type="textarea" id="nf-password" name="nf-password" placeholder="Enter Text.."
                           value={this.state.content}
                           onChange={evt=>this.setState({content:evt.target.value})}
                    />
                    <FormText className="help-block">Please enter your response</FormText>
                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.responseReclamation.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>

              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
