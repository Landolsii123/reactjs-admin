import React, { Component } from 'react';
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      dateReservation: "",
      nombrePlace:"",
      etat:"",
      reservation:{
        dateReservation: "",
        nombrePlace:"",
        etat:""
      }
    }
  }

  componentDidMount(){
    this.getReservation();

  }

  setReponse(){
    fetch('http://localhost:8080/Reservation/setResponse/'+localStorage.getItem("idReservation")+"/"+this.state.etat, {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        window.location.href="/#/home/reservation"
      });

  }


  getReservation(){
    fetch('http://localhost:8080/Reservation/one/'+localStorage.getItem("idReservation"), {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({reservation: data});
      });
  }


  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }

  render() {
    return (
      <div className="animated fadeIn">

        <Row>
          <Col xs="12" md="12">

            <Card>
              <CardHeader>
                <strong>Response</strong> reservation
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="nf-email">Date</Label>
                    <Input type="date" id="nf-email" name="nf-email" autoComplete="email"
                           value={this.state.reservation.dateReservation}
                    />
                    <FormText className="help-block">Client Reservation</FormText>
                  </FormGroup><FormGroup>
                    <Label htmlFor="nf-email">Number of Place</Label>
                    <Input type="number" id="nf-email" name="nf-email" autoComplete="email"
                           value={this.state.reservation.nombrePlace}
                    />
                    <FormText className="help-block">Client Reservation</FormText>
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="nf-password">Response</Label>

                    <Col xs="12" md="9" size="lg">
                      <Input type="select" name="selectLg" id="selectLg" bsSize="lg"
                             value={this.state.etat} onChange={evt=>this.setState({etat:evt.target.value})}>
                        <option value="0">{this.state.reservation.etat}</option>

                            <option value="Accepted">Confirmed</option>
                            <option value="Refused">Refused</option>

                      </Input>
                    </Col>
                    <FormText className="help-block">Please enter your response</FormText>
                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.setReponse.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>

              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
