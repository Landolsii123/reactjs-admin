import React, { Component } from 'react';
import  axios  from 'axios';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

class Login extends Component {
  constructor(){
    super();
    this.state={login:"",password:""}
}
componentDidMount(){
    console.log('didmount');
}

componentWillMount(){
    console.log("will mount");
}
  login(){
    console.log("state",this.state);
    axios.post("http://localhost:8080/Personne/login" , {
      login: this.state.login,
      password:this.state.password
    }).then(res => {
      console.log("data ",res.data);
      if(res.data['data']===null){
        alert ("votre login ou mot de passe est invalide")
      }
      else if (res.data['data']['role'] === "Admin"){
        localStorage.setItem("idPer",res.data['data']['id'])

        window.location.href="/#/home/category"
      }
      else{
        alert ("votre login ou mot de passe est invalide")
      }
    })
  }
  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Username" autoComplete="username" value={this.state.login}
                        onChange={evt=>this.setState({login:evt.target.value})}/>
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="current-password" value={this.state.password}
                        onChange={evt=>this.setState({password:evt.target.value})}/>
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" onClick={this.login.bind(this)}>Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>

              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
