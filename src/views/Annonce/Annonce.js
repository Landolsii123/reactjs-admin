import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import PlacesAutocomplete from 'react-places-autocomplete';
import {
  geocodeByAddress,
  geocodeByPlaceId,
  getLatLng,
} from 'react-places-autocomplete';

let prev = 0;

let next = 0;

let last = 0;

let first = 0;
class Tables extends Component {
  constructor(){
    super();
    this.state={annonce:[],
      currentPage: 1,
      todosPerPage: 5
    }

    this.handleClick = this.handleClick.bind(this);
    this.handleLastClick = this.handleLastClick.bind(this);
    this.handleFirstClick = this.handleFirstClick.bind(this);
  }

  handleClick(event) {

    event.preventDefault();

    this.setState({

      currentPage: Number(event.target.id)

    });

  }


  handleLastClick(event) {
    event.preventDefault();
    this.setState({

      currentPage:last

    });

  }


  handleFirstClick(event) {
    event.preventDefault();
    this.setState({

      currentPage:1

    });

  }



  handleChange = address => {
    this.setState({ address });
  }

  handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => console.log('Success', latLng))
      .catch(error => console.error('Error', error));
  };

  componentDidMount(){
    this.getAllAnnonce();
  }

  getAllAnnonce(){
    fetch('http://localhost:8080/Annonce/all', {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({annonce: data});
      });
  }

  handleClickEdit(e,id){
    e.preventDefault();
    console.log("id ",id)
    localStorage.setItem("idAnn",id);
    window.location.href="/#/home/updateAnnonce"
  }

  handleClickRemove(e,id){
    e.preventDefault();
    console.log("id ",id);
    this.removeAnnonce(id);
  }

  removeAnnonce(id){
    fetch('http://localhost:8080/Annonce/delete/'+id, {method: 'DELETE'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.getAllAnnonce();
      });
  }


  render() {

    let {annonce, currentPage, todosPerPage} = this.state;
    // Logic for displaying current todos

    let indexOfLastTodo = currentPage * todosPerPage;
    let indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    let currentTodos = annonce.slice(indexOfFirstTodo, indexOfLastTodo);

    prev = currentPage > 0 ? (currentPage - 1) : 0;
    last = Math.ceil(annonce.length / todosPerPage);
    next = (last === currentPage) ? currentPage : currentPage + 1;

    // Logic for displaying page numbers

    let pageNumbers = [];

    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }
    return (
      <div className="animated fadeIn">

        <Row>


          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Annonce List
              </CardHeader>
              <CardBody>
                <Table responsive striped>
                  <thead>
                  <tr>
                    <th>Photo</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Details</th>
                    <th>Adress</th>
                    <th>City</th>
                    <th>Date</th>
                    <th>Reservation Number</th>
                    <th>Price</th>

                    <th>Admin Name</th>
                    <th>Category</th>
                    <th>Update</th>
                    <th>Delete</th>
                  </tr>
                  </thead>
                  <tbody>

                  {

                    currentTodos.map((item,index) =>{
                      return(
                        <tr key={index}>
                        <td><img src={`http://localhost:8080/files/${item.photo}`} width="50" height="50" /></td>
                        <td>{item.title}</td>
                        <td>{item.description}</td>
                        <td>{item.detail}</td>
                        <td>{item.adress}</td>
                        <td>{item.ville}</td>
                        <td>{item.dateAnnonce}</td>
                        <td>{item.nombreReservation}</td>
                        <td>{item.prix}</td>
                        <td>{item.photo}</td>
                        <td>{item.personne.name}</td>
                        <td>{item.category.catName}</td>
                        <td><i class="fas fa-edit" style={{color:"blue"}} onClick={evt=>this.handleClickEdit(evt,item.id)}></i></td>
                        <td><i class="fas fa-trash-alt" style={{color:"red"}} onClick={evt=>this.handleClickRemove(evt,item.id)}></i></td>
                      </tr>
                      );
                    })
                  }
                  </tbody>
                </Table>
                <nav>
                  <Pagination>
                    <PaginationItem>

                      { prev === 0 ? <PaginationLink disabled>First</PaginationLink> :

                        <PaginationLink onClick={this.handleFirstClick} id={prev} href={prev}>First</PaginationLink>

                      }

                    </PaginationItem>

                    <PaginationItem>

                      { prev === 0 ? <PaginationLink disabled>Prev</PaginationLink> :

                        <PaginationLink onClick={this.handleClick} id={prev} href={prev}>Prev</PaginationLink>

                      }

                    </PaginationItem>

                    {

                      pageNumbers.map((number,i) =>

                        <Pagination key= {i}>

                          <PaginationItem active = {pageNumbers[currentPage-1] === (number) ? true : false} >

                            <PaginationLink onClick={this.handleClick} href={number} key={number} id={number}>

                              {number}

                            </PaginationLink>

                          </PaginationItem>

                        </Pagination>
                      )}

                    <PaginationItem>

                      {

                        currentPage === last ? <PaginationLink disabled>Next</PaginationLink> :

                          <PaginationLink onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Next</PaginationLink>

                      }

                    </PaginationItem>
                    <PaginationItem>

                      {

                        currentPage === last ? <PaginationLink disabled>Last</PaginationLink> :

                          <PaginationLink onClick={this.handleLastClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Last</PaginationLink>

                      }

                    </PaginationItem>

                  </Pagination>

                </nav>
              </CardBody>
            </Card>
          </Col>
        </Row>


      </div>

    );
  }
}

export default Tables;
