import React, { Component } from 'react';
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      title:"",
      description:"",
      detail:"",
      adress:"",
      ville:"",
      lattitude:"",
      longitude:"",
      dateAnnonce:"",
      nombreReservation:"",
      prix:"",
      photo:"",
      category:[],
      idCat:"",
      address:"",
      file:File,
    };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }

  componentDidMount(){
    this.getAllCategory();
  }

  getAllCategory(){
    fetch('http://localhost:8080/Categorie/all', {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({category: data});
      });
  }

  addAnnonce(){

     const title = this.state.title;
      const description=this.state.description;
      const detail=this.state.detail;
      const adress= this.state.adress;
      const ville=this.state.ville;
      const lattitude= this.state.lattitude;
      const longitude= this.state.longitude;
      const dateAnnonce= this.state.dateAnnonce;
      const nombreReservation= this.state.nombreReservation;
      const prix= this.state.prix;
      const photo= this.state.file.name;

    ///ajout annonce avec fetch
    const option = {
      method: "POST",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({
        "photo": "" + photo + "",
        "title": "" + title + "",
        "description": "" + description + "",
        "detail": "" + detail + "",
        "adress": "" + adress + "",
        "ville": "" + ville + "",
        "lattitude": "" + lattitude + "",
        "longitude": "" + longitude + "",
        "dateAnnonce": "" + dateAnnonce + "",
        "nombreReservation": "" + nombreReservation + "",
        "prix": "" + prix + ""
      })}
      //ajout photo
      const config = {
        "Content-Type": "multipart/form-data"
      }

      const formData = new FormData();
    formData.append('file', this.state.file);
    console.log("photo ", this.state.file);
    // add annonce
    fetch("http://localhost:8080/Annonce/add/"+localStorage.getItem("idPer")+"/"+this.state.idCat, option)
      .then(response => response.json())
      .then(data => {
        console.log(data);


        //axios pour ajout file
        axios.post("http://localhost:8080/post/", formData, config)
          .then(function (res) {
            console.log(res.data)

          })

        window.location.href="/#/home/annonce"

      })

  }

  handleChange = adress => {
    this.setState({ adress });
  };

  handleSelect = (addresse) => {
    geocodeByAddress(addresse)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
          this.setState({longitude: JSON.parse(JSON.stringify((latLng))).lat})
          this.setState({lattitude: JSON.parse(JSON.stringify((latLng))).lng})
          console.log('Success', latLng)

        }
      )


      .catch(error => console.error('Error', error))
  }

  handlechangefile =(e)=>{
    const fil=e.target.files[0];
    this.setState({file:fil});
  }

  render() {
    return (
      <div className="animated fadeIn">

        <Row>

          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <strong>Annonce</strong> Add
              </CardHeader>
              <CardBody>

                <Form action="" method="post" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="selectLg">Adresse</Label>
                    </Col>
                    <Col xs="12" md="9" size="lg">
                    <PlacesAutocomplete
                      value={this.state.adress}
                      onChange={this.handleChange}
                      onSelect={this.handleSelect}
                    >
                      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                        <div>
                          <input
                            {...getInputProps({
                              placeholder: 'Search Places ...',
                              className: 'location-search-input',
                            })}
                          />
                          <div className="autocomplete-dropdown-container">
                            {loading && <div>Loading...</div>}
                            {suggestions.map(suggestion => {
                              const className = suggestion.active
                                ? 'suggestion-item--active'
                                : 'suggestion-item';
                              // inline style for demonstration purpose
                              const style = suggestion.active
                                ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                : { backgroundColor: '#ffffff', cursor: 'pointer' };
                              return (
                                <div
                                  {...getSuggestionItemProps(suggestion, {
                                    className,
                                    style,
                                  })}
                                >
                                  <span>{suggestion.description}</span>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                      )}
                    </PlacesAutocomplete>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="selectLg">Select Category</Label>
                    </Col>
                    <Col xs="12" md="9" size="lg">
                      <Input type="select" name="selectLg" id="selectLg" bsSize="lg"
                      value={this.state.idCat} onChange={evt=>this.setState({idCat:evt.target.value})}>
                        {<option value="0">Please select</option>}
                        {
                          this.state.category.map((item)=>
                            <option value={item.id}>{item.catName}</option>

                          )
                        }

                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Title</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             value={this.state.title}
                             onChange={evt=>this.setState({title:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Description</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             value={this.state.description}
                             onChange={evt=>this.setState({description:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Details</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             value={this.state.detail}
                             onChange={evt=>this.setState({detail:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Address</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             value={this.state.adress}
                             onChange={evt=>this.setState({adress:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">City</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             value={this.state.ville}
                             onChange={evt=>this.setState({ville:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Date</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="date" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             value={this.state.dateAnnonce}
                             onChange={evt=>this.setState({dateAnnonce:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                    <Label htmlFor="hf-email">Reservation Number</Label>
                  </Col>
                    <Col xs="12" md="9">
                      <Input type="number" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             value={this.state.nombreReservation}
                             onChange={evt=>this.setState({nombreReservation:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Price</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="number" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             value={this.state.prix}
                             onChange={evt=>this.setState({prix:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Photo</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="file" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"

                             onChange={this.handlechangefile}
                      />

                    </Col>

                  </FormGroup>

                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.addAnnonce.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
