import React, { Component } from 'react';
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";
import PlacesAutocomplete from 'react-places-autocomplete';
import {
  geocodeByAddress,
  geocodeByPlaceId,
  getLatLng,
} from 'react-places-autocomplete';

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      title:"",
      description:"",
      detail:"",
      adress:"",
      ville:"",
      lattitude:"",
      longitude:"",
      dateAnnonce:"",
      nombreReservation:"",
      prix:"",
      photo:"",
      annonce:{
        title:"",
        description:"",
        detail:"",
        adress:"",
        ville:"",
        lattitude:"",
        longitude:"",
        dateAnnonce:"",
        nombreReservation:"",
        prix:"",
        photo:""
      },
      category:[],
      idCat:"",
      file:File
    };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }

  componentDidMount() {
    this.getAnn();
    this.getAllCategory();
  }
  getAllCategory(){
    fetch('http://localhost:8080/Categorie/all', {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({category: data});
      });
  }

  getAnn(){
    fetch('http://localhost:8080/Annonce/one/'+localStorage.getItem("idAnn"), {method: 'GET'})
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({annonce: data});
        this.setState({idCat: data['category']['id']});
        this.setState({photo: data['photo']});
      });
  }



  updateAnnonce(){
    if(this.state.title === ""){
      this.state.title = this.state.annonce.title;
    }if(this.state.description === ""){
      this.state.description = this.state.annonce.description;
    }if(this.state.detail === ""){
      this.state.detail = this.state.annonce.detail;
    }if(this.state.adress === ""){
      this.state.adress = this.state.annonce.adress;
    }if(this.state.ville === ""){
      this.state.ville = this.state.annonce.ville;
    }if(this.state.lattitude === ""){
      this.state.lattitude = this.state.annonce.lattitude;
    }if(this.state.longitude === ""){
      this.state.longitude = this.state.annonce.longitude;
    }if(this.state.dateAnnonce === ""){
      this.state.dateAnnonce = this.state.annonce.dateAnnonce;
    }if(this.state.nombreReservation === ""){
      this.state.nombreReservation = this.state.annonce.nombreReservation;
    }if(this.state.prix === ""){
      this.state.prix = this.state.annonce.prix;
    }
    else{
      const title = this.state.title;
      const description=this.state.description;
      const detail=this.state.detail;
      const adress= this.state.adress;
      const ville=this.state.ville;
      const lattitude= this.state.lattitude;
      const longitude= this.state.longitude;
      const dateAnnonce= this.state.dateAnnonce;
      const nombreReservation= this.state.nombreReservation;
      const prix= this.state.prix;
      const photo= this.state.photo;

      ///ajout annonce avec fetch
      const option = {
        method: "PUT",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
          "photo": "" + photo + "",
          "title": "" + title + "",
          "description": "" + description + "",
          "detail": "" + detail + "",
          "adress": "" + adress + "",
          "ville": "" + ville + "",
          "lattitude": "" + lattitude + "",
          "longitude": "" + longitude + "",
          "dateAnnonce": "" + dateAnnonce + "",
          "nombreReservation": "" + nombreReservation + "",
          "prix": "" + prix + ""
        })}
      //ajout photo
      const config = {
        "Content-Type": "multipart/form-data"
      }

      const formData = new FormData();
      formData.append('file', this.state.file);
      console.log("photo ", this.state.file);
      // add annonce
      fetch("http://localhost:8080/Annonce/update/"+localStorage.getItem("idAnn")+"/"+localStorage.getItem("idPer")+"/"+this.state.idCat , option)
        .then(response => response.json())
        .then(data => {
          console.log(data);


          if(this.state.file.name != 'File'){
            //axios pour ajout file
            axios.post("http://localhost:8080/post/", formData, config)
              .then(function (res) {
                console.log(res.data)

              })
          }


          window.location.href="/#/home/annonce"

        })
    }
  }

  handleChange = adress => {
    this.setState({ adress });
  };

  handleSelect = (addresse) => {
    geocodeByAddress(addresse)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
          this.setState({longitude: JSON.parse(JSON.stringify((latLng))).lat})
          this.setState({lattitude: JSON.parse(JSON.stringify((latLng))).lng})
          console.log('Success', latLng)

        }
      )


      .catch(error => console.error('Error', error))
  }

  handlechangefile =(e)=>{
    const fil=e.target.files[0];
    this.setState({file:fil});
    try{
      this.setState({photo:fil.name});
    }
    catch(E){
      this.setState({photo: this.state.annonce.photo})
    }
  }

  render() {
    return (
      <div className="animated fadeIn">

        <Row>

          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <strong>Annonce</strong> Update
              </CardHeader>
              <CardBody>
                <Form action="" method="post" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Photo</Label>
                    </Col>
                    <Col md="3">
                      <img src={`http://localhost:8080/files/${this.state.photo}`} width="50" height="50" />                    </Col>
                    <Col xs="12" md="6">
                      <Input type="file" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             onChange={this.handlechangefile}
                      />

                    </Col>

                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="selectLg">Adresse</Label>
                    </Col>
                    <Col xs="12" md="9" size="lg">
                      <PlacesAutocomplete
                        value={this.state.adress}
                        onChange={this.handleChange}
                        onSelect={this.handleSelect}
                      >
                        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                          <div>
                            <input
                              {...getInputProps({
                                placeholder: 'Search Places ...',
                                className: 'location-search-input',
                              })}
                            />
                            <div className="autocomplete-dropdown-container">
                              {loading && <div>Loading...</div>}
                              {suggestions.map(suggestion => {
                                const className = suggestion.active
                                  ? 'suggestion-item--active'
                                  : 'suggestion-item';
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                  ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                  : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                return (
                                  <div
                                    {...getSuggestionItemProps(suggestion, {
                                      className,
                                      style,
                                    })}
                                  >
                                    <span>{suggestion.description}</span>
                                  </div>
                                );
                              })}
                            </div>
                          </div>
                        )}
                      </PlacesAutocomplete>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="selectLg">Select Category</Label>
                    </Col>
                    <Col xs="12" md="9" size="lg">
                      <Input type="select" name="selectLg" id="selectLg" bsSize="lg"
                      value={this.state.idCat} onChange={evt=>this.setState({idCat:evt.target.value})}>
                        {<option value="0">Please select</option>}
                        {
                          this.state.category.map((item)=>
                            <option value={item.id}>{item.catName}</option>

                          )
                        }

                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Title</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             defaultValue={this.state.annonce.title}
                             onChange={evt=>this.setState({title:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Description</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             defaultValue={this.state.annonce.description}
                             onChange={evt=>this.setState({description:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Details</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             defaultValue={this.state.annonce.detail}
                             onChange={evt=>this.setState({detail:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Address</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             defaultValue={this.state.annonce.adress}
                             onChange={evt=>this.setState({adress:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">City</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             defaultValue={this.state.annonce.ville}
                             onChange={evt=>this.setState({ville:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Date</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="date" id="date" name="date"
                             defaultValue={this.state.annonce.dateAnnonce}
                             onChange={evt=>this.setState({dateAnnonce:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                    <Label htmlFor="hf-email">Reservation Number</Label>
                  </Col>
                    <Col xs="12" md="9">
                      <Input type="number" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             defaultValue={this.state.annonce.nombreReservation}
                             onChange={evt=>this.setState({nombreReservation:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-email">Price</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="number" id="hf-email" name="hf-email" placeholder="Enter The Name..." autoComplete="email"
                             defaultValue={this.state.annonce.prix}
                             onChange={evt=>this.setState({prix:evt.target.value})}
                      />

                    </Col>
                  </FormGroup>


                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.updateAnnonce.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
